/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import static java.lang.Math.*;

/**
 *
 * @author Estudiantes
 */
public class OperaciónTrigonometrica {
    
    double numero;
    double seno(){
        
        return sin(numero);
    }
    
    double coseno(){
        
        double numeroRad=toRadians(numero);
        return cos(numero);
    }
    
    double tangente(){
        
        return tan(numero);
    }
    
}
